import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';

import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  constructor(private storage: Storage, public alertController: AlertController) { }

  

  titre = "Un titre whououououo";
  maVariable = "Texte avec des cornichons";
  
  inputs = [];
  ngOnInit() {
    this.retrieveStorageData();
  }

  unTexteRandom(){
    return "Ta maman est si grosse qu'elle rentre pas dans une database";
  }

  async retrieveStorageData(){
    // Async
    const data = await this.storage.get('inputs');
    this.inputs = data ?? [];

    //Callback
    // this.storage.get('inputs').then((val) => {
    //   this.inputs = val ?? [];
    // });
  }
  storeVariable(){
    this.inputs.push(this.maVariable);
    console.log(this.inputs);
    // set a key/value
    this.storage.set('inputs', this.inputs);

    // Or to get a key/value pair
   
  }


  deleteInput(index){
    console.log(index);
    this.inputs.splice(index, 1);
    this.storage.set('inputs', this.inputs);

  }

  deleteAllInputs(){
    this.presentAlertMultipleButtons()
      
    
    
  }

  async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
      header: 'Suppression des données',
      subHeader: 'Vous allez tout supprimer',
      message: 'Etes-vous sûr de vouloir effacer vos données ?',
      buttons: [{
        text: 'Annuler',
        handler: () => {
          console.log('Confirm Cancel');
        }
      },{
        text: 'Supprimer mes données',
        handler: () => {
          console.log('Confirm Ok');
          this.inputs=[];
      this.storage.clear();
        }
      }]
    });

    await alert.present();
  }

  
}
