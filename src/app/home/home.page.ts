import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private router : Router) {}

  titre = "Ceci est une ôde à l'artichaut";

  alert(text){
    alert(text);
  }

  openDetails(){
    this.router.navigateByUrl("/details");
  }
}
